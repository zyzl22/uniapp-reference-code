'use strict';
// 收藏功能
const db = uniCloud.database()
const dbCmd = db.command

exports.main = async (event, context) => {

	const {
		user_id,
		article_id
	} = event;

	const userinfo = await db.collection('user').doc(user_id).get()
	const article_id_ids = userinfo.data[0].article_likes_ids
	let dbCmdFuns = null
	if (article_id_ids.includes(article_id)) {
		//已经收藏的话,就变成 未收藏
		dbCmdFuns = dbCmd.pull(article_id) // 取消收藏
	} else {
		dbCmdFuns = dbCmd.addToSet(article_id) //收藏
	}

	await db.collection('user').doc(user_id).update({
		'article_likes_ids': dbCmdFuns
	})

	//返回数据给客户端
	return {
		code: 200,
		msg: '数据请求成功',
		data: userinfo.data[0]
	}
};
