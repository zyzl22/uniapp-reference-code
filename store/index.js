// vuex 状态管理
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
	// 数据源
	state: {
		userinfo: uni.getStorageSync('USERINFO') || {},
		historyLists: uni.getStorageSync("__hostory") || []
	},
	//可以改变数据源中的数据
	mutations: {
		SET_USER_INFO(state, userinfo) {
			state.userinfo = userinfo
		},
		SET_HISTORY_LISTS(state, history) {
			state.historyLists = history
		},
		CLEAR_HISTORY(state, history) {
			state.historyLists = []
		}
	},
	actions: {
		set_userinfo({commit}, userinfo) {
			uni.setStorageSync('USERINFO', userinfo)
			commit('SET_USER_INFO', userinfo)
		},
		set_history({
			commit,
			state
		}, history) {
			let list = state.historyLists
			list.unshift(history)
			uni.setStorageSync('__hostory', list)
			commit('SET_HISTORY_LISTS', list)
		},
		clear_history({
			commit
		}) {
			uni.removeStorageSync('__hostory')
			commit('CLEAR_HISTORY')
		}
	}
})

export default store
